//
//  ViewController.swift
//  Balls
//
//  Created by Александра Лесничая on 17.01.22.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        let sizeOfArea = CGSize(width: 400, height: 400)
        let area = SquareArea(size: sizeOfArea, color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        area.setBalls(withColors: [.blue, .red, .green, .yellow], andRadius: 40)
        view.addSubview(area)
    }
}
